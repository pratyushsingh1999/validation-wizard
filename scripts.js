window.addEventListener('load' , () => {
    const acc = document.getElementById("nav1");
    const profile = document.getElementById("nav2");
    const warning = document.getElementById("nav3");
    const finish = document.getElementById("nav4");
    const form = document.getElementById('form-content');
    const prevButton = document.getElementById('previous');
    const nextButton = document.getElementById('next');

    acc.addEventListener('click' , () => {
        if (!acc.classList.contains('nav-active')) {
            acc.classList.add('nav-active');
            acc.classList.remove('nav-tab');
            form.innerHTML = accHtml;
        }
        profile.classList.remove('nav-active');
        profile.classList.add('nav-tab');
        warning.classList.remove('nav-active');
        warning.classList.add('nav-tab');
        finish.classList.remove('nav-active');
        finish.classList.add('nav-tab');
    })
    profile.addEventListener('click' , () => {
        if (!profile.classList.contains('nav-active')) {
            profile.classList.add('nav-active');
            profile.classList.remove('nav-tab');
            form.innerHTML = profileHtml;
        }
        acc.classList.remove('nav-active');
        acc.classList.add('nav-tab');
        warning.classList.remove('nav-active');
        warning.classList.add('nav-tab');
        finish.classList.remove('nav-active');
        finish.classList.add('nav-tab');
    })
    warning.addEventListener('click' , () => {
        if (!warning.classList.contains('nav-active')) {
            warning.classList.add('nav-active');
            warning.classList.remove('nav-tab');
            form.innerHTML = warningHtml;
        }
        profile.classList.remove('nav-active');
        profile.classList.add('nav-tab');
        acc.classList.remove('nav-active');
        acc.classList.add('nav-tab');
        finish.classList.remove('nav-active');
        finish.classList.add('nav-tab');
    })
    finish.addEventListener('click' , () => {
        if (!finish.classList.contains('nav-active')) {
            finish.classList.add('nav-active');
            finish.classList.remove('nav-tab');
            form.innerHTML = finishHtml;
        }
        profile.classList.remove('nav-active');
        profile.classList.add('nav-tab');
        warning.classList.remove('nav-active');
        warning.classList.add('nav-tab');
        acc.classList.remove('nav-active');
        acc.classList.add('nav-tab');
    })
    prevButton.addEventListener('click' , () => {
        if (acc.classList.contains('nav-active')) {
            // do nothing
        }
        if (profile.classList.contains('nav-active')) {
            acc.classList.add('nav-active');
            acc.classList.remove('nav-tab');
            profile.classList.add('nav-tab');
            profile.classList.remove('nav-active');
            form.innerHTML = accHtml;
        }
        if (warning.classList.contains('nav-active')) {
            profile.classList.add('nav-active');
            profile.classList.remove('nav-tab');
            warning.classList.add('nav-tab');
            warning.classList.remove('nav-active');
            form.innerHTML = profileHtml;
        }
        if (finish.classList.contains('nav-active')) {
            warning.classList.add('nav-active');
            warning.classList.remove('nav-tab');
            finish.classList.add('nav-tab');
            finish.classList.remove('nav-active');
            form.innerHTML = warningHtml;
        }
    })
    nextButton.addEventListener('click', () => {
        if (warning.classList.contains('nav-active')) {
            finish.classList.add('nav-active');
            finish.classList.remove('nav-tab');
            warning.classList.add('nav-tab');
            warning.classList.remove('nav-active');
            form.innerHTML = finishHtml;
        }
        if (profile.classList.contains('nav-active')) {
            warning.classList.add('nav-active');
            warning.classList.remove('nav-tab');
            profile.classList.add('nav-tab');
            profile.classList.remove('nav-active');
            form.innerHTML = warningHtml;
        }
        if (acc.classList.contains('nav-active')) {
            profile.classList.add('nav-active');
            profile.classList.remove('nav-tab');
            acc.classList.remove('nav-active');
            acc.classList.add('nav-tab');
            form.innerHTML = profileHtml;
        }

        if (finish.classList.contains('nav-active')) {
            // do nothing
        }
    })
})
const accHtml= `<p>Account Information</p>
                <label>Username *</label>
                <input type="email">
                <label>Password *</label>
                <input type="password">
                <label>Confirm Password *</label>
                <input type="password">`
const profileHtml = `<p>Profile Information</p>
                <label>First Name *</label>
                <input type="text">
                <label>Last Name *</label>
                <input type="text">
                <label>Age *</label>
                <input type="number">`
const warningHtml = `<p>Are you sure you want to register with us?</p>`
const finishHtml = `<p>Thank you for registering with us, may you have a nice day</p>`
